function greetRegular(name) {
    return "Hello, "+ name;
}

greetRegular('ram');

var greetArrow = (name) => {
    return "Hello ," + name;
};

const greetMin = (name:string) => "Hello, "+ name;
console.log(greetMin('Aravind'));