interface Person{
    fname:string,
    lname:string,
    age?:number
}

    let employee1:Person = {
        fname:"aravind",
        lname:"a",
        age:26
    }
    let employee2:Person = {
        fname:"Ram",
        lname:"a"
    } 
    console.log(employee2);