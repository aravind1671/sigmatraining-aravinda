function greetRegular(name) {
    return "Hello, " + name;
}
greetRegular('ram');
var greetArrow = function (name) {
    return "Hello ," + name;
};
var greetMin = function (name) { return "Hello, " + name; };
console.log(greetMin('Aravind'));
